from airflow.operators.python import PythonOperator
from airflow.operators.email import EmailOperator
from airflow.utils.email import send_email
from airflow import DAG
from pathlib import Path
import pandas as pd
import datetime
import pendulum
import json
import os

PATH = str(Path(os.path.dirname(__file__)).parent)

default_args = {
    "owner": "jeff",
    "email": ["s9012126000@gmail.com"],
    "email_on_failure": False,
}


def checker(path, tag, code, msg, file_num):
    num = 0
    wrong_files = []
    for p in Path(path).rglob('*_lvr_land_*.csv'):
        check = tag
        file_name = str(p).split('/')[-1]
        if file_name[0] in check and file_name[-5] == code:
            num += 1
        else:
            wrong_files.append(file_name)
    if num != file_num:
        send_email(to="s9012126000@gmail.com",
                   subject='Airflow alert <real_estate_ETL> Error',
                   html_content=f"""
                   <h3>Time: {datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")}</h3>
                   <h3>Lost of {msg} csv file:</h3>
                   <p>{json.dumps(wrong_files)}</p>
                    """)
        raise FileExistsError


def check_csv():
    checker(
        f'{PATH}/real_estate',
        {'F', 'A', 'E'},
        'A',
        'real_estate',
        12
    )
    checker(
        f'{PATH}/pre_sale',
        {'B', 'H'},
        'B',
        'pre_sale',
        8
    )
    print("file check accomplished")


def read_df(dir_name, tag, ls):
    for path in Path(dir_name).rglob('*_lvr_land_*.csv'):
        path = str(path)
        df_name = path.split('/')[-2].replace("S", "_") + "_" + path.split('/')[-1][0]+'_' + tag
        df = pd.read_csv(path, header=[1])
        df["df_name"] = df_name
        ls.append(df)


def merge_df(ls):
    tol_df = None
    for df in ls:
        if tol_df is None:
            tol_df = df
        else:
            tol_df = pd.concat([tol_df, df])
    return tol_df


def estate_ETL():
    df_ls = []
    read_df(f'{PATH}/real_estate', "A", df_ls)
    read_df(f'{PATH}/pre_sale', "B", df_ls)
    df_all = merge_df(df_ls)
    df_all.to_csv(f'{PATH}/df_all.csv', encoding='utf-16')


def estate_analysis():
    df = pd.read_csv(f'{PATH}/df_all.csv', low_memory=False, encoding='utf-16')
    tol_count = len(df)
    berth_count = df['transaction pen number'].str.slice(-1).astype(int).sum()
    average_price = round(df['total price NTD'].sum()/tol_count, 2)
    average_berth_price = round(df['the berth total price NTD'].sum()/berth_count, 2)
    result = {
        'tol_count': [tol_count],
        'berth_count': [berth_count],
        'average_price': [average_price],
        'average_berth_price': [average_berth_price]
    }
    result = pd.DataFrame(result)
    result.to_csv(f'{PATH}/count.csv')


with DAG(
    dag_id="estate_ETL_analysis",
    schedule_interval="00 00 5 * *",
    start_date=pendulum.datetime(2022, 1, 1, tz="Asia/Taipei"),
    catchup=False,
    default_args=default_args,
    max_active_runs=1,
    dagrun_timeout=datetime.timedelta(minutes=10),
) as dag:

    file_checker = PythonOperator(
        task_id="file_checker",
        python_callable=check_csv
    )

    estate_ETL = PythonOperator(
        task_id="estate_ETL",
        python_callable=estate_ETL,
        dag=dag
    )

    estate_analysis = PythonOperator(
        task_id='estate_analysis',
        python_callable=estate_analysis,
        dag=dag
    )

    send_success_email = EmailOperator(
        task_id='send_success_email',
        to='s9012126000@gmail.com',
        subject='Airflow alert <real_estate_ETL> Success',
        html_content=f"""
        <h3>Time: {datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")}</h3>
        <h3>Job real_estate_ETL accomplished!</h3>""",
        dag=dag
    )

    file_checker >> estate_ETL >> estate_analysis >> send_success_email


